name = main

all: $(name)_en_color.pdf

allver: $(name)_en_color.pdf $(name)_en_bw.pdf $(name)_fr_color.pdf $(name)_fr_bw.pdf $(name)_du_color.pdf $(name)_du_bw.pdf

$(name)_en_color.pdf: lang_en_a color_a compile_a

$(name)_en_bw.pdf: lang_en_b bw_a compile_b

$(name)_fr_color.pdf: lang_fr_a color_b compile_c

$(name)_fr_bw.pdf: lang_fr_b bw_b compile_d

$(name)_du_color.pdf: lang_du_a color_c compile_e

$(name)_du_bw.pdf: lang_du_b bw_c compile_f

lang_en_%: $(name).tex
	$(eval lang = 'en')
	sed -i '/\\newcommand{\\Eng}\[1\]{/c\\\newcommand{\\Eng}[1]{#1}' main.tex
	sed -i '/\\newcommand{\\Fra}\[1\]{/c\\\newcommand{\\Fra}[1]{}' main.tex
	sed -i '/\\newcommand{\\Dut}\[1\]{/c\\\newcommand{\\Dut}[1]{}' main.tex

lang_fr_%: $(name).tex
	$(eval lang = 'fr')
	sed -i '/\\newcommand{\\Eng}\[1\]{/c\\\newcommand{\\Eng}[1]{}' main.tex
	sed -i '/\\newcommand{\\Fra}\[1\]{/c\\\newcommand{\\Fra}[1]{#1}' main.tex
	sed -i '/\\newcommand{\\Dut}\[1\]{/c\\\newcommand{\\Dut}[1]{}' main.tex

lang_du_%: $(name).tex
	$(eval lang = 'du')
	sed -i '/\\newcommand{\\Eng}\[1\]{/c\\\newcommand{\\Eng}[1]{}' main.tex
	sed -i '/\\newcommand{\\Fra}\[1\]{/c\\\newcommand{\\Fra}[1]{}' main.tex
	sed -i '/\\newcommand{\\Dut}\[1\]{/c\\\newcommand{\\Dut}[1]{#1}' main.tex

bw_%: $(name).tex 
	$(eval color = 'bw')
	sed -i '/\%\\selectcolormodel{gray}/c\\\selectcolormodel{gray}' main.tex

color_%: $(name).tex
	$(eval color = 'color')
	sed -i '/\\selectcolormodel{gray}/c\\%\\selectcolormodel{gray}' main.tex

compile_%: $(name).tex
	latex $(name).tex
	latex $(name).tex
	dvips $(name).dvi
	mkdir -p PDF/
	ps2pdf $(name).ps PDF/whist_$(lang)_$(color).pdf
